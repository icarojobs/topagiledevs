-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2020 at 12:19 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `topagiledevs`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_developers`
--

CREATE TABLE `admin_developers` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_favorites`
--

CREATE TABLE `admin_favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_url` longtext COLLATE utf8mb4_unicode_ci,
  `bio` longtext COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `public_repos` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `followers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `since` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `blog` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_favorites`
--

INSERT INTO `admin_favorites` (`id`, `username`, `name`, `avatar_url`, `bio`, `type`, `company`, `location`, `email`, `public_repos`, `followers`, `since`, `blog`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '10up', '10up', 'https://avatars3.githubusercontent.com/u/3358927?v=4', 'We make finely crafted websites and tools for content creators, doing our part to create a better web for clients like Microsoft, Time, ESPN, and Adobe.', 'Organization', '', 'United States', 'pr@10up.com', '113', '0', '2013-01-23T20:39:25Z', 'https://10up.com', '2020-03-15 17:09:13', '2020-03-15 17:09:13', NULL),
(2, 'matomo-org', 'Matomo Analytics', 'https://avatars1.githubusercontent.com/u/698038?v=4', 'Open Source Analytics Piwik is now Matomo', 'Organization', '', 'Worldwide', 'hello@matomo.org', '65', '0', '2011-03-29T20:11:53Z', 'https://matomo.org', '2020-03-15 17:15:14', '2020-03-15 17:15:14', NULL),
(3, 'b2evolution', 'b2evolution Group', 'https://avatars2.githubusercontent.com/u/1178992?v=4', '#Bootstrap #RWD #CMS #multiblog #forums #email #marketing #social', 'Organization', '', '', '', '173', '0', '2011-11-07T20:56:43Z', 'http://b2evolution.net/', '2020-03-15 17:45:08', '2020-03-15 17:45:08', NULL),
(4, '2600hz', '2600Hz KAZOO and Monster UI', 'https://avatars3.githubusercontent.com/u/408801?v=4', 'Turn-key Telco', 'Organization', '', 'San Francisco, CA', '', '93', '0', '2010-09-20T19:19:44Z', 'http://www.2600hz.org', '2020-03-16 12:49:34', '2020-03-16 12:49:34', NULL),
(5, 'Adamdad', 'Xingyi Yang', 'https://avatars0.githubusercontent.com/u/26020510?v=4', 'Adam Yang(Xingyi Yang)\r\nMaster of UCSD\r\n\r\nMachine learner', 'User', '@UCSD @seu', 'La Jolla, California, US', '', '31', '42', '2017-02-25T07:19:17Z', 'adamdad.github.io', '2020-03-16 13:08:59', '2020-03-16 13:08:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_menu`
--

CREATE TABLE `admin_menu` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uri` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_menu`
--

INSERT INTO `admin_menu` (`id`, `parent_id`, `order`, `title`, `icon`, `uri`, `permission`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 'Dashboard', 'fa-bar-chart', '/stats', NULL, NULL, '2020-03-16 12:53:37'),
(2, 0, 4, 'Admin', 'fa-tasks', '', NULL, NULL, '2020-03-15 15:15:11'),
(3, 2, 5, 'Users', 'fa-users', 'auth/users', NULL, NULL, '2020-03-15 15:15:11'),
(4, 2, 6, 'Roles', 'fa-user', 'auth/roles', NULL, NULL, '2020-03-15 15:15:11'),
(5, 2, 7, 'Permission', 'fa-ban', 'auth/permissions', NULL, NULL, '2020-03-15 15:15:11'),
(6, 2, 8, 'Menu', 'fa-bars', 'auth/menu', NULL, NULL, '2020-03-15 15:15:11'),
(7, 2, 13, 'Operation log', 'fa-history', 'auth/logs', NULL, NULL, '2020-03-15 15:15:11'),
(8, 0, 14, 'Helpers', 'fa-gears', '', NULL, '2020-03-15 05:06:29', '2020-03-15 15:15:11'),
(9, 8, 15, 'Scaffold', 'fa-keyboard-o', 'helpers/scaffold', NULL, '2020-03-15 05:06:29', '2020-03-15 15:15:11'),
(10, 8, 16, 'Database terminal', 'fa-database', 'helpers/terminal/database', NULL, '2020-03-15 05:06:29', '2020-03-15 15:15:11'),
(11, 8, 17, 'Laravel artisan', 'fa-terminal', 'helpers/terminal/artisan', NULL, '2020-03-15 05:06:29', '2020-03-15 15:15:11'),
(12, 8, 18, 'Routes', 'fa-list-alt', 'helpers/routes', NULL, '2020-03-15 05:06:29', '2020-03-15 15:15:11'),
(13, 2, 9, 'Scaffold', 'fa-briefcase', '/helpers/scaffold', '*', '2020-03-15 05:09:37', '2020-03-15 15:15:11'),
(14, 2, 12, 'Database CLI', 'fa-database', '/helpers/terminal/database', '*', '2020-03-15 05:12:22', '2020-03-15 15:15:11'),
(15, 2, 10, 'Artisan CLI', 'fa-terminal', '/helpers/terminal/artisan', '*', '2020-03-15 05:13:27', '2020-03-15 15:15:11'),
(16, 2, 11, 'Route List', 'fa-chain-broken', '/helpers/routes', '*', '2020-03-15 05:14:25', '2020-03-15 15:15:11'),
(17, 0, 2, 'Pesquisar', 'fa-search', '/search', '*', '2020-03-15 05:57:04', '2020-03-15 06:00:59'),
(18, 0, 3, 'Favoritos', 'fa-star-o', '/favorites', '*', '2020-03-15 15:14:52', '2020-03-15 15:15:11');

-- --------------------------------------------------------

--
-- Table structure for table `admin_operation_log`
--

CREATE TABLE `admin_operation_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `input` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_operation_log`
--

INSERT INTO `admin_operation_log` (`id`, `user_id`, `path`, `method`, `ip`, `input`, `created_at`, `updated_at`) VALUES
(90, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:49:15', '2020-03-15 05:49:15'),
(91, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"admin_developers\",\"model_name\":\"App\\\\\\\\Models\\\\\\\\Developer\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\DeveloperController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"username\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"OKAqv3oA3gvARF8VoBfxowFoLDGXgAKZQTGh5dty\"}', '2020-03-15 05:49:48', '2020-03-15 05:49:48'),
(92, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-03-15 05:49:48', '2020-03-15 05:49:48'),
(93, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:53:47', '2020-03-15 05:53:47'),
(94, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-15 05:56:17', '2020-03-15 05:56:17'),
(95, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:56:33', '2020-03-15 05:56:33'),
(96, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Favoritos\",\"icon\":\"fa-star\",\"uri\":\"\\/favorites\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"OKAqv3oA3gvARF8VoBfxowFoLDGXgAKZQTGh5dty\"}', '2020-03-15 05:57:04', '2020-03-15 05:57:04'),
(97, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 05:57:05', '2020-03-15 05:57:05'),
(98, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"OKAqv3oA3gvARF8VoBfxowFoLDGXgAKZQTGh5dty\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":17},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":13},{\\\"id\\\":15},{\\\"id\\\":16},{\\\"id\\\":14},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12}]}]\"}', '2020-03-15 05:57:11', '2020-03-15 05:57:11'),
(99, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:57:11', '2020-03-15 05:57:11'),
(100, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 05:57:13', '2020-03-15 05:57:13'),
(101, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:57:25', '2020-03-15 05:57:25'),
(102, 1, 'admin/favorites/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:57:29', '2020-03-15 05:57:29'),
(103, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:57:53', '2020-03-15 05:57:53'),
(104, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:58:18', '2020-03-15 05:58:18'),
(105, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:58:20', '2020-03-15 05:58:20'),
(106, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:58:23', '2020-03-15 05:58:23'),
(107, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:59:48', '2020-03-15 05:59:48'),
(108, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 05:59:57', '2020-03-15 05:59:57'),
(109, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 06:00:10', '2020-03-15 06:00:10'),
(110, 1, 'admin/auth/menu/17/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 06:00:16', '2020-03-15 06:00:16'),
(111, 1, 'admin/auth/menu/17', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Pesquisar\",\"icon\":\"fa-search\",\"uri\":\"\\/search\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"OKAqv3oA3gvARF8VoBfxowFoLDGXgAKZQTGh5dty\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/auth\\/menu\"}', '2020-03-15 06:00:59', '2020-03-15 06:00:59'),
(112, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 06:00:59', '2020-03-15 06:00:59'),
(113, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 06:01:11', '2020-03-15 06:01:11'),
(114, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 06:01:16', '2020-03-15 06:01:16'),
(115, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:03:31', '2020-03-15 06:03:31'),
(116, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:04:12', '2020-03-15 06:04:12'),
(117, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:06:02', '2020-03-15 06:06:02'),
(118, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:08:57', '2020-03-15 06:08:57'),
(119, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:13:38', '2020-03-15 06:13:38'),
(120, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:16:03', '2020-03-15 06:16:03'),
(121, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:18:39', '2020-03-15 06:18:39'),
(122, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:19:15', '2020-03-15 06:19:15'),
(123, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:21:03', '2020-03-15 06:21:03'),
(124, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:21:27', '2020-03-15 06:21:27'),
(125, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:23:32', '2020-03-15 06:23:32'),
(126, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:24:49', '2020-03-15 06:24:49'),
(127, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:28:53', '2020-03-15 06:28:53'),
(128, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:30:13', '2020-03-15 06:30:13'),
(129, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:30:33', '2020-03-15 06:30:33'),
(130, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:30:49', '2020-03-15 06:30:49'),
(131, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:31:29', '2020-03-15 06:31:29'),
(132, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:32:56', '2020-03-15 06:32:56'),
(133, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:37:12', '2020-03-15 06:37:12'),
(134, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:40:34', '2020-03-15 06:40:34'),
(135, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:41:06', '2020-03-15 06:41:06'),
(136, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:42:56', '2020-03-15 06:42:56'),
(137, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:43:40', '2020-03-15 06:43:40'),
(138, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 06:44:07', '2020-03-15 06:44:07'),
(139, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:45:18', '2020-03-15 06:45:18'),
(140, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:47:32', '2020-03-15 06:47:32'),
(141, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:48:01', '2020-03-15 06:48:01'),
(142, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:48:57', '2020-03-15 06:48:57'),
(143, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:51:52', '2020-03-15 06:51:52'),
(144, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:54:41', '2020-03-15 06:54:41'),
(145, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:54:54', '2020-03-15 06:54:54'),
(146, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:55:04', '2020-03-15 06:55:04'),
(147, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:56:37', '2020-03-15 06:56:37'),
(148, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 06:58:57', '2020-03-15 06:58:57'),
(149, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:01:46', '2020-03-15 07:01:46'),
(150, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:03:34', '2020-03-15 07:03:34'),
(151, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:15:16', '2020-03-15 07:15:16'),
(152, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:15:58', '2020-03-15 07:15:58'),
(153, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:16:22', '2020-03-15 07:16:22'),
(154, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:16:48', '2020-03-15 07:16:48'),
(155, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:17:22', '2020-03-15 07:17:22'),
(156, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:18:26', '2020-03-15 07:18:26'),
(157, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:19:29', '2020-03-15 07:19:29'),
(158, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:21:01', '2020-03-15 07:21:01'),
(159, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:33:30', '2020-03-15 07:33:30'),
(160, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:34:20', '2020-03-15 07:34:20'),
(161, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:34:58', '2020-03-15 07:34:58'),
(162, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:35:51', '2020-03-15 07:35:51'),
(163, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:45:03', '2020-03-15 07:45:03'),
(164, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:45:33', '2020-03-15 07:45:33'),
(165, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:45:59', '2020-03-15 07:45:59'),
(166, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:46:32', '2020-03-15 07:46:32'),
(167, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:47:01', '2020-03-15 07:47:01'),
(168, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:47:33', '2020-03-15 07:47:33'),
(169, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:48:56', '2020-03-15 07:48:56'),
(170, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:51:25', '2020-03-15 07:51:25'),
(171, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:55:39', '2020-03-15 07:55:39'),
(172, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:56:10', '2020-03-15 07:56:10'),
(173, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:56:53', '2020-03-15 07:56:53'),
(174, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:57:59', '2020-03-15 07:57:59'),
(175, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:58:56', '2020-03-15 07:58:56'),
(176, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 07:59:34', '2020-03-15 07:59:34'),
(177, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:00:03', '2020-03-15 08:00:03'),
(178, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:00:30', '2020-03-15 08:00:30'),
(179, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:01:13', '2020-03-15 08:01:13'),
(180, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:02:11', '2020-03-15 08:02:11'),
(181, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:04:06', '2020-03-15 08:04:06'),
(182, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:05:32', '2020-03-15 08:05:32'),
(183, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:05:57', '2020-03-15 08:05:57'),
(184, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:13:22', '2020-03-15 08:13:22'),
(185, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:14:29', '2020-03-15 08:14:29'),
(186, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:17:31', '2020-03-15 08:17:31'),
(187, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:20:23', '2020-03-15 08:20:23'),
(188, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:21:07', '2020-03-15 08:21:07'),
(189, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:22:00', '2020-03-15 08:22:00'),
(190, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:27:59', '2020-03-15 08:27:59'),
(191, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:32:23', '2020-03-15 08:32:23'),
(192, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:34:06', '2020-03-15 08:34:06'),
(193, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:34:43', '2020-03-15 08:34:43'),
(194, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:35:55', '2020-03-15 08:35:55'),
(195, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:37:10', '2020-03-15 08:37:10'),
(196, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:37:29', '2020-03-15 08:37:29'),
(197, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:38:28', '2020-03-15 08:38:28'),
(198, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 08:39:21', '2020-03-15 08:39:21'),
(199, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:39:37', '2020-03-15 08:39:37'),
(200, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:40:43', '2020-03-15 08:40:43'),
(201, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:41:52', '2020-03-15 08:41:52'),
(202, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:46:15', '2020-03-15 08:46:15'),
(203, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:47:33', '2020-03-15 08:47:33'),
(204, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:49:08', '2020-03-15 08:49:08'),
(205, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:49:36', '2020-03-15 08:49:36'),
(206, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:50:21', '2020-03-15 08:50:21'),
(207, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:52:06', '2020-03-15 08:52:06'),
(208, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:54:57', '2020-03-15 08:54:57'),
(209, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:55:58', '2020-03-15 08:55:58'),
(210, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 08:58:53', '2020-03-15 08:58:53'),
(211, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 09:00:25', '2020-03-15 09:00:25'),
(212, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 09:03:49', '2020-03-15 09:03:49'),
(213, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 12:52:38', '2020-03-15 12:52:38'),
(214, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 12:53:56', '2020-03-15 12:53:56'),
(215, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 12:54:29', '2020-03-15 12:54:29'),
(216, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:12:05', '2020-03-15 13:12:05'),
(217, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:13:01', '2020-03-15 13:13:01'),
(218, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:15:37', '2020-03-15 13:15:37'),
(219, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:15:55', '2020-03-15 13:15:55'),
(220, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:16:12', '2020-03-15 13:16:12'),
(221, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:20:14', '2020-03-15 13:20:14'),
(222, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\",\"0\":[\"Tiger Nixon\",\"System Architect\",\"Edinburgh\"],\"_\":\"1584267616303\"}', '2020-03-15 13:20:23', '2020-03-15 13:20:23'),
(223, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:21:59', '2020-03-15 13:21:59'),
(224, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:29:51', '2020-03-15 13:29:51'),
(225, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:31:09', '2020-03-15 13:31:09'),
(226, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:33:33', '2020-03-15 13:33:33'),
(227, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:34:19', '2020-03-15 13:34:19'),
(228, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:35:52', '2020-03-15 13:35:52'),
(229, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:39:11', '2020-03-15 13:39:11'),
(230, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:41:45', '2020-03-15 13:41:45'),
(231, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:42:31', '2020-03-15 13:42:31'),
(232, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:44:01', '2020-03-15 13:44:01'),
(233, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:46:38', '2020-03-15 13:46:38'),
(234, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:48:19', '2020-03-15 13:48:19'),
(235, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:49:55', '2020-03-15 13:49:55'),
(236, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:53:14', '2020-03-15 13:53:14'),
(237, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:54:41', '2020-03-15 13:54:41'),
(238, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:55:12', '2020-03-15 13:55:12'),
(239, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:55:29', '2020-03-15 13:55:29'),
(240, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:55:43', '2020-03-15 13:55:43'),
(241, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:56:22', '2020-03-15 13:56:22'),
(242, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:57:12', '2020-03-15 13:57:12'),
(243, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:58:17', '2020-03-15 13:58:17'),
(244, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 13:59:18', '2020-03-15 13:59:18'),
(245, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:00:00', '2020-03-15 14:00:00'),
(246, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:00:24', '2020-03-15 14:00:24'),
(247, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:01:04', '2020-03-15 14:01:04'),
(248, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:01:25', '2020-03-15 14:01:25'),
(249, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:01:49', '2020-03-15 14:01:49'),
(250, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:02:12', '2020-03-15 14:02:12'),
(251, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:02:24', '2020-03-15 14:02:24'),
(252, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:02:40', '2020-03-15 14:02:40'),
(253, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:03:18', '2020-03-15 14:03:18'),
(254, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:04:21', '2020-03-15 14:04:21'),
(255, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:04:34', '2020-03-15 14:04:34'),
(256, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:04:50', '2020-03-15 14:04:50'),
(257, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:06:48', '2020-03-15 14:06:48'),
(258, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:07:23', '2020-03-15 14:07:23'),
(259, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:14:06', '2020-03-15 14:14:06'),
(260, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:15:02', '2020-03-15 14:15:02'),
(261, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:16:26', '2020-03-15 14:16:26'),
(262, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:17:26', '2020-03-15 14:17:26'),
(263, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:18:47', '2020-03-15 14:18:47'),
(264, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:19:39', '2020-03-15 14:19:39'),
(265, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:20:33', '2020-03-15 14:20:33'),
(266, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:21:28', '2020-03-15 14:21:28'),
(267, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:22:24', '2020-03-15 14:22:24'),
(268, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:24:59', '2020-03-15 14:24:59'),
(269, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:31:31', '2020-03-15 14:31:31'),
(270, 1, 'admin/search', 'GET', '127.0.0.1', '{\"project\":\"website\",\"language\":\"php\",\"order\":\"desc\"}', '2020-03-15 14:38:19', '2020-03-15 14:38:19'),
(271, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:43:01', '2020-03-15 14:43:01'),
(272, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:43:39', '2020-03-15 14:43:39'),
(273, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:44:58', '2020-03-15 14:44:58'),
(274, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:45:47', '2020-03-15 14:45:47'),
(275, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:47:06', '2020-03-15 14:47:06'),
(276, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:48:19', '2020-03-15 14:48:19'),
(277, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 14:49:51', '2020-03-15 14:49:51'),
(278, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:04:09', '2020-03-15 15:04:09'),
(279, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:04:38', '2020-03-15 15:04:38'),
(280, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:05:58', '2020-03-15 15:05:58'),
(281, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:06:15', '2020-03-15 15:06:15'),
(282, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:07:34', '2020-03-15 15:07:34'),
(283, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:07:59', '2020-03-15 15:07:59'),
(284, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:08:38', '2020-03-15 15:08:38'),
(285, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:09:15', '2020-03-15 15:09:15'),
(286, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:10:09', '2020-03-15 15:10:09'),
(287, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:10:45', '2020-03-15 15:10:45'),
(288, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:12:46', '2020-03-15 15:12:46'),
(289, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:13:57', '2020-03-15 15:13:57'),
(290, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:13:58', '2020-03-15 15:13:58'),
(291, 1, 'admin/auth/permissions', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:14:06', '2020-03-15 15:14:06'),
(292, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:14:13', '2020-03-15 15:14:13'),
(293, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Favoritos\",\"icon\":\"fa-star-o\",\"uri\":\"\\/favorites\",\"roles\":[\"1\",null],\"permission\":\"*\",\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\"}', '2020-03-15 15:14:52', '2020-03-15 15:14:52'),
(294, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 15:14:52', '2020-03-15 15:14:52'),
(295, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 15:14:55', '2020-03-15 15:14:55'),
(296, 1, 'admin/auth/menu', 'POST', '127.0.0.1', '{\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\",\"_order\":\"[{\\\"id\\\":1},{\\\"id\\\":17},{\\\"id\\\":18},{\\\"id\\\":2,\\\"children\\\":[{\\\"id\\\":3},{\\\"id\\\":4},{\\\"id\\\":5},{\\\"id\\\":6},{\\\"id\\\":13},{\\\"id\\\":15},{\\\"id\\\":16},{\\\"id\\\":14},{\\\"id\\\":7}]},{\\\"id\\\":8,\\\"children\\\":[{\\\"id\\\":9},{\\\"id\\\":10},{\\\"id\\\":11},{\\\"id\\\":12}]}]\"}', '2020-03-15 15:15:11', '2020-03-15 15:15:11'),
(297, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:15:12', '2020-03-15 15:15:12'),
(298, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 15:15:14', '2020-03-15 15:15:14'),
(299, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:18:29', '2020-03-15 15:18:29'),
(300, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:19:07', '2020-03-15 15:19:07'),
(301, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:19:16', '2020-03-15 15:19:16'),
(302, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:19:33', '2020-03-15 15:19:33'),
(303, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:19:36', '2020-03-15 15:19:36'),
(304, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:19:58', '2020-03-15 15:19:58'),
(305, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:20:18', '2020-03-15 15:20:18'),
(306, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:20:33', '2020-03-15 15:20:33'),
(307, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:22:42', '2020-03-15 15:22:42'),
(308, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:25:38', '2020-03-15 15:25:38'),
(309, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:27:01', '2020-03-15 15:27:01'),
(310, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:33:55', '2020-03-15 15:33:55'),
(311, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:36:52', '2020-03-15 15:36:52'),
(312, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:39:26', '2020-03-15 15:39:26'),
(313, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:39:32', '2020-03-15 15:39:32'),
(314, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:40:49', '2020-03-15 15:40:49'),
(315, 1, 'admin/helpers/scaffold', 'POST', '127.0.0.1', '{\"table_name\":\"admin_favorites\",\"model_name\":\"App\\\\\\\\Models\\\\\\\\Favorite\",\"controller_name\":\"App\\\\Admin\\\\Controllers\\\\FavoriteController\",\"create\":[\"migration\",\"model\",\"controller\",\"migrate\"],\"fields\":[{\"name\":\"username\",\"type\":\"string\",\"key\":\"unique\",\"default\":null,\"comment\":null},{\"name\":\"bio\",\"type\":\"longText\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"type\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"company\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"location\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"email\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"public_repos\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"followers\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"since\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null},{\"name\":\"blog\",\"type\":\"string\",\"nullable\":\"on\",\"key\":null,\"default\":null,\"comment\":null}],\"timestamps\":\"on\",\"soft_deletes\":\"on\",\"primary_key\":\"id\",\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\"}', '2020-03-15 15:45:22', '2020-03-15 15:45:22'),
(316, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-03-15 15:45:22', '2020-03-15 15:45:22'),
(317, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:46:45', '2020-03-15 15:46:45'),
(318, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-15 15:46:52', '2020-03-15 15:46:52'),
(319, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:47:10', '2020-03-15 15:47:10'),
(320, 1, 'admin/favorites/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:47:20', '2020-03-15 15:47:20'),
(321, 1, 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:48:14', '2020-03-15 15:48:14'),
(322, 1, 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"make:migration add_avatar_to_admin_favorites --table=admin_favorites\",\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\"}', '2020-03-15 15:48:52', '2020-03-15 15:48:52'),
(323, 1, 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate\",\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\"}', '2020-03-15 15:50:58', '2020-03-15 15:50:58'),
(324, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:51:06', '2020-03-15 15:51:06'),
(325, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-15 15:51:11', '2020-03-15 15:51:11'),
(326, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-15 15:52:37', '2020-03-15 15:52:37'),
(327, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-15 15:54:10', '2020-03-15 15:54:10'),
(328, 1, 'admin/favorites/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:54:16', '2020-03-15 15:54:16'),
(329, 1, 'admin/favorites/create', 'GET', '127.0.0.1', '[]', '2020-03-15 15:55:16', '2020-03-15 15:55:16'),
(330, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 15:55:27', '2020-03-15 15:55:27'),
(331, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 15:55:31', '2020-03-15 15:55:31'),
(332, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:01:40', '2020-03-15 16:01:40'),
(333, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:02:31', '2020-03-15 16:02:31'),
(334, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:06:23', '2020-03-15 16:06:23'),
(335, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:09:08', '2020-03-15 16:09:08'),
(336, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:12:29', '2020-03-15 16:12:29'),
(337, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:14:30', '2020-03-15 16:14:30'),
(338, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:16:51', '2020-03-15 16:16:51'),
(339, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:18:04', '2020-03-15 16:18:04'),
(340, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:28:37', '2020-03-15 16:28:37'),
(341, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:29:33', '2020-03-15 16:29:33'),
(342, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:30:28', '2020-03-15 16:30:28'),
(343, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:32:01', '2020-03-15 16:32:01'),
(344, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:34:39', '2020-03-15 16:34:39'),
(345, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:35:04', '2020-03-15 16:35:04'),
(346, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:35:59', '2020-03-15 16:35:59'),
(347, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:36:51', '2020-03-15 16:36:51'),
(348, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:38:30', '2020-03-15 16:38:30'),
(349, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:43:04', '2020-03-15 16:43:04'),
(350, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 16:46:33', '2020-03-15 16:46:33'),
(351, 1, 'admin/helpers/terminal/artisan', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 16:53:51', '2020-03-15 16:53:51'),
(352, 1, 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"make:migration add_name_to_favorites_table --table=admin_favorites\",\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\"}', '2020-03-15 16:54:12', '2020-03-15 16:54:12'),
(353, 1, 'admin/helpers/terminal/artisan', 'POST', '127.0.0.1', '{\"c\":\"migrate\",\"_token\":\"XcD074rWeiMJJ7Cr0LwLHwKi28sEUJXH8ThsMIhB\"}', '2020-03-15 16:55:02', '2020-03-15 16:55:02'),
(354, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:00:20', '2020-03-15 17:00:20'),
(355, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:01:20', '2020-03-15 17:01:20'),
(356, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:01:26', '2020-03-15 17:01:26'),
(357, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:01:31', '2020-03-15 17:01:31'),
(358, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:08:40', '2020-03-15 17:08:40'),
(359, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-15 17:09:28', '2020-03-15 17:09:28'),
(360, 1, 'admin/favorites/1', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:09:36', '2020-03-15 17:09:36'),
(361, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:09:50', '2020-03-15 17:09:50'),
(362, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:14:27', '2020-03-15 17:14:27'),
(363, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:15:51', '2020-03-15 17:15:51'),
(364, 1, 'admin/favorites/2/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:15:56', '2020-03-15 17:15:56'),
(365, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:18:08', '2020-03-15 17:18:08'),
(366, 1, 'admin/favorites/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:18:10', '2020-03-15 17:18:10'),
(367, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:18:13', '2020-03-15 17:18:13'),
(368, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-15 17:20:42', '2020-03-15 17:20:42'),
(369, 1, 'admin/favorites/2', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:20:50', '2020-03-15 17:20:50'),
(370, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:21:21', '2020-03-15 17:21:21'),
(371, 1, 'admin/favorites/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:21:25', '2020-03-15 17:21:25'),
(372, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:21:35', '2020-03-15 17:21:35'),
(373, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:21:39', '2020-03-15 17:21:39'),
(374, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\",\"_export_\":\"all\"}', '2020-03-15 17:21:47', '2020-03-15 17:21:47'),
(375, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:22:08', '2020-03-15 17:22:08'),
(376, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:22:14', '2020-03-15 17:22:14'),
(377, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:23:01', '2020-03-15 17:23:01'),
(378, 1, 'admin/auth/menu/1/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:23:05', '2020-03-15 17:23:05'),
(379, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:23:31', '2020-03-15 17:23:31'),
(380, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:23:33', '2020-03-15 17:23:33'),
(381, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:23:42', '2020-03-15 17:23:42'),
(382, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-15 17:24:06', '2020-03-15 17:24:06'),
(383, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:24:13', '2020-03-15 17:24:13'),
(384, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:24:30', '2020-03-15 17:24:30'),
(385, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:24:39', '2020-03-15 17:24:39'),
(386, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:24:41', '2020-03-15 17:24:41'),
(387, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:25:13', '2020-03-15 17:25:13'),
(388, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-15 17:25:24', '2020-03-15 17:25:24'),
(389, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:25:30', '2020-03-15 17:25:30'),
(390, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:25:36', '2020-03-15 17:25:36'),
(391, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:25:39', '2020-03-15 17:25:39'),
(392, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:25:48', '2020-03-15 17:25:48'),
(393, 1, 'admin/favorites/create', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:25:57', '2020-03-15 17:25:57'),
(394, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:43:06', '2020-03-15 17:43:06'),
(395, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:43:34', '2020-03-15 17:43:34'),
(396, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:43:36', '2020-03-15 17:43:36'),
(397, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:44:37', '2020-03-15 17:44:37'),
(398, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-15 17:44:39', '2020-03-15 17:44:39'),
(399, 1, 'admin/favorites', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:45:14', '2020-03-15 17:45:14'),
(400, 1, 'admin/favorites/3/edit', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:45:21', '2020-03-15 17:45:21'),
(401, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-15 17:45:26', '2020-03-15 17:45:26'),
(402, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:09:08', '2020-03-16 12:09:08'),
(403, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:09:29', '2020-03-16 12:09:29'),
(404, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:12:39', '2020-03-16 12:12:39'),
(405, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:12:45', '2020-03-16 12:12:45'),
(406, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:12:53', '2020-03-16 12:12:53'),
(407, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:12:57', '2020-03-16 12:12:57'),
(408, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:13:09', '2020-03-16 12:13:09'),
(409, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:13:20', '2020-03-16 12:13:20'),
(410, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:13:22', '2020-03-16 12:13:22'),
(411, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:23:35', '2020-03-16 12:23:35'),
(412, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:24:08', '2020-03-16 12:24:08'),
(413, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:29:28', '2020-03-16 12:29:28'),
(414, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:29:35', '2020-03-16 12:29:35'),
(415, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:29:47', '2020-03-16 12:29:47'),
(416, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:29:53', '2020-03-16 12:29:53'),
(417, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:30:17', '2020-03-16 12:30:17'),
(418, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:30:25', '2020-03-16 12:30:25'),
(419, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:30:29', '2020-03-16 12:30:29'),
(420, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:30:32', '2020-03-16 12:30:32'),
(421, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:30:35', '2020-03-16 12:30:35'),
(422, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:32:13', '2020-03-16 12:32:13'),
(423, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:32:16', '2020-03-16 12:32:16'),
(424, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:33:46', '2020-03-16 12:33:46'),
(425, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:33:58', '2020-03-16 12:33:58'),
(426, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:34:14', '2020-03-16 12:34:14'),
(427, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:34:19', '2020-03-16 12:34:19'),
(428, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:35:34', '2020-03-16 12:35:34'),
(429, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:35:41', '2020-03-16 12:35:41'),
(430, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:36:09', '2020-03-16 12:36:09'),
(431, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:36:23', '2020-03-16 12:36:23'),
(432, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:36:55', '2020-03-16 12:36:55'),
(433, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:37:25', '2020-03-16 12:37:25'),
(434, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:37:35', '2020-03-16 12:37:35'),
(435, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:38:31', '2020-03-16 12:38:31'),
(436, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:40:10', '2020-03-16 12:40:10'),
(437, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:40:10', '2020-03-16 12:40:10'),
(438, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:40:12', '2020-03-16 12:40:12'),
(439, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:40:30', '2020-03-16 12:40:30'),
(440, 1, 'admin/search', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:40:33', '2020-03-16 12:40:33'),
(441, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:43:17', '2020-03-16 12:43:17'),
(442, 1, 'admin', 'GET', '127.0.0.1', '{\"_pjax\":\"#pjax-container\"}', '2020-03-16 12:45:58', '2020-03-16 12:45:58'),
(443, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:46:01', '2020-03-16 12:46:01'),
(444, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:46:39', '2020-03-16 12:46:39'),
(445, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:47:56', '2020-03-16 12:47:56'),
(446, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:48:10', '2020-03-16 12:48:10'),
(447, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '[]', '2020-03-16 12:48:47', '2020-03-16 12:48:47'),
(448, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:48:55', '2020-03-16 12:48:55'),
(449, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:49:06', '2020-03-16 12:49:06'),
(450, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-16 12:49:40', '2020-03-16 12:49:40'),
(451, 1, 'admin/favorites/4', 'GET', '127.0.0.1', '[]', '2020-03-16 12:49:48', '2020-03-16 12:49:48'),
(452, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 12:49:58', '2020-03-16 12:49:58'),
(453, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:50:20', '2020-03-16 12:50:20'),
(454, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-16 12:50:27', '2020-03-16 12:50:27'),
(455, 1, 'admin/helpers/scaffold', 'GET', '127.0.0.1', '[]', '2020-03-16 12:51:18', '2020-03-16 12:51:18'),
(456, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-16 12:53:16', '2020-03-16 12:53:16'),
(457, 1, 'admin/auth/menu/1/edit', 'GET', '127.0.0.1', '[]', '2020-03-16 12:53:21', '2020-03-16 12:53:21'),
(458, 1, 'admin/auth/menu/1', 'PUT', '127.0.0.1', '{\"parent_id\":\"0\",\"title\":\"Dashboard\",\"icon\":\"fa-bar-chart\",\"uri\":\"\\/stats\",\"roles\":[null],\"permission\":null,\"_token\":\"xxfx9Or5YGp5mWPjtHoOLznxEdvABNqog37AorkY\",\"_method\":\"PUT\",\"_previous_\":\"http:\\/\\/localhost:8000\\/admin\\/auth\\/menu\"}', '2020-03-16 12:53:37', '2020-03-16 12:53:37'),
(459, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-16 12:53:37', '2020-03-16 12:53:37'),
(460, 1, 'admin/auth/menu', 'GET', '127.0.0.1', '[]', '2020-03-16 12:53:44', '2020-03-16 12:53:44'),
(461, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 12:53:49', '2020-03-16 12:53:49'),
(462, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 12:53:54', '2020-03-16 12:53:54'),
(463, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 12:59:12', '2020-03-16 12:59:12'),
(464, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:01:02', '2020-03-16 13:01:02'),
(465, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:01:50', '2020-03-16 13:01:50'),
(466, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:07:15', '2020-03-16 13:07:15'),
(467, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 13:07:25', '2020-03-16 13:07:25'),
(468, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-16 13:07:56', '2020-03-16 13:07:56'),
(469, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:07:59', '2020-03-16 13:07:59'),
(470, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:08:06', '2020-03-16 13:08:06'),
(471, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-16 13:08:12', '2020-03-16 13:08:12'),
(472, 1, 'admin/search', 'GET', '127.0.0.1', '[]', '2020-03-16 13:08:32', '2020-03-16 13:08:32'),
(473, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:09:05', '2020-03-16 13:09:05'),
(474, 1, 'admin/favorites', 'GET', '127.0.0.1', '[]', '2020-03-16 13:09:13', '2020-03-16 13:09:13'),
(475, 1, 'admin/favorites/5', 'GET', '127.0.0.1', '[]', '2020-03-16 13:09:25', '2020-03-16 13:09:25'),
(476, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:09:34', '2020-03-16 13:09:34'),
(477, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 13:09:41', '2020-03-16 13:09:41'),
(478, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '[]', '2020-03-16 13:10:05', '2020-03-16 13:10:05'),
(479, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 13:24:53', '2020-03-16 13:24:53'),
(480, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:25:03', '2020-03-16 13:25:03'),
(481, 1, 'admin/auth/users', 'GET', '127.0.0.1', '[]', '2020-03-16 13:25:10', '2020-03-16 13:25:10'),
(482, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '[]', '2020-03-16 13:25:40', '2020-03-16 13:25:40'),
(483, 1, 'admin', 'GET', '127.0.0.1', '[]', '2020-03-16 13:25:59', '2020-03-16 13:25:59'),
(484, 1, 'admin/stats', 'GET', '127.0.0.1', '[]', '2020-03-16 13:26:06', '2020-03-16 13:26:06'),
(485, 1, 'admin/auth/logout', 'GET', '127.0.0.1', '[]', '2020-03-16 13:27:15', '2020-03-16 13:27:15');

-- --------------------------------------------------------

--
-- Table structure for table `admin_permissions`
--

CREATE TABLE `admin_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `http_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `http_path` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_permissions`
--

INSERT INTO `admin_permissions` (`id`, `name`, `slug`, `http_method`, `http_path`, `created_at`, `updated_at`) VALUES
(1, 'All permission', '*', '', '*', NULL, NULL),
(2, 'Dashboard', 'dashboard', 'GET', '/', NULL, NULL),
(3, 'Login', 'auth.login', '', '/auth/login\r\n/auth/logout', NULL, NULL),
(4, 'User setting', 'auth.setting', 'GET,PUT', '/auth/setting', NULL, NULL),
(5, 'Auth management', 'auth.management', '', '/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs', NULL, NULL),
(6, 'Admin helpers', 'ext.helpers', '', '/helpers/*', '2020-03-15 05:06:29', '2020-03-15 05:06:29');

-- --------------------------------------------------------

--
-- Table structure for table `admin_roles`
--

CREATE TABLE `admin_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_roles`
--

INSERT INTO `admin_roles` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'administrator', '2020-03-15 04:47:28', '2020-03-15 04:47:28');

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_menu`
--

CREATE TABLE `admin_role_menu` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_menu`
--

INSERT INTO `admin_role_menu` (`role_id`, `menu_id`, `created_at`, `updated_at`) VALUES
(1, 2, NULL, NULL),
(1, 13, NULL, NULL),
(1, 14, NULL, NULL),
(1, 15, NULL, NULL),
(1, 16, NULL, NULL),
(1, 17, NULL, NULL),
(1, 18, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_permissions`
--

CREATE TABLE `admin_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_permissions`
--

INSERT INTO `admin_role_permissions` (`role_id`, `permission_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_role_users`
--

CREATE TABLE `admin_role_users` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_role_users`
--

INSERT INTO `admin_role_users` (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `manager_id` int(10) UNSIGNED DEFAULT NULL,
  `username` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `manager_id`, `username`, `password`, `name`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, NULL, 'admin', '$2y$10$a43Hla86e162UfCktF5zDOPxvIzrNEqurPjWl6fBGlHMU0DGt0dsK', 'Administrator', NULL, '9wmjW5ahsKS4LjDrxaF46pW3O024Ykb3DnHmhJHCDYUyqxiAv9eRH5YJFVYP', '2020-03-15 04:47:28', '2020-03-15 04:47:28');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user_permissions`
--

CREATE TABLE `admin_user_permissions` (
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_04_173148_create_admin_tables', 1),
(4, '2020_03_15_021847_add_manager_id_to_admin_users', 2),
(6, '2020_03_15_024948_create_admin_developers_table', 3),
(7, '2020_03_15_124522_create_admin_favorites_table', 4),
(8, '2020_03_15_124853_add_avatar_to_admin_favorites', 5),
(9, '2020_03_15_135412_add_name_to_favorites_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_developers`
--
ALTER TABLE `admin_developers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_developers_username_unique` (`username`);

--
-- Indexes for table `admin_favorites`
--
ALTER TABLE `admin_favorites`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_favorites_username_unique` (`username`);

--
-- Indexes for table `admin_menu`
--
ALTER TABLE `admin_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_operation_log_user_id_index` (`user_id`);

--
-- Indexes for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_permissions_name_unique` (`name`),
  ADD UNIQUE KEY `admin_permissions_slug_unique` (`slug`);

--
-- Indexes for table `admin_roles`
--
ALTER TABLE `admin_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_roles_name_unique` (`name`),
  ADD UNIQUE KEY `admin_roles_slug_unique` (`slug`);

--
-- Indexes for table `admin_role_menu`
--
ALTER TABLE `admin_role_menu`
  ADD KEY `admin_role_menu_role_id_menu_id_index` (`role_id`,`menu_id`);

--
-- Indexes for table `admin_role_permissions`
--
ALTER TABLE `admin_role_permissions`
  ADD KEY `admin_role_permissions_role_id_permission_id_index` (`role_id`,`permission_id`);

--
-- Indexes for table `admin_role_users`
--
ALTER TABLE `admin_role_users`
  ADD KEY `admin_role_users_role_id_user_id_index` (`role_id`,`user_id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_username_unique` (`username`);

--
-- Indexes for table `admin_user_permissions`
--
ALTER TABLE `admin_user_permissions`
  ADD KEY `admin_user_permissions_user_id_permission_id_index` (`user_id`,`permission_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_developers`
--
ALTER TABLE `admin_developers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `admin_favorites`
--
ALTER TABLE `admin_favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `admin_menu`
--
ALTER TABLE `admin_menu`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `admin_operation_log`
--
ALTER TABLE `admin_operation_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=486;

--
-- AUTO_INCREMENT for table `admin_permissions`
--
ALTER TABLE `admin_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `admin_roles`
--
ALTER TABLE `admin_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
