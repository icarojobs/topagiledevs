$('#pjax-container').attr('id', 'app');

var app = new Vue({
    el: '#app',
    data: {
      title: 'TOP AGILE DEVS',
      baseUrl: $('#base_url').val(),
      project: '',
      language: '',
      order: '',
      developers: [],
      devDetails: {},
      hasResult: false,
      isLoading: false,
      showDetail: false
    },
    methods: {
      getDevelopers: async function (event) {
        this.isLoading = true
        this.hasResult = false
        this.developers = []
        const totalResult = 500
        
        const {data} = await axios.get(`https://api.github.com/search/repositories?q=${this.project}+language:${this.language}&sort=stars&order=${this.order}&page=1&per_page=${totalResult}`)

        if(!!data.items && data.items.length > 0) {
          this.hasResult = true

          let jsonData = data.items.map((dev) => {
            return `<tr><td><a class="cursor-dev" onclick="app.devDetail('${dev.owner.login}')">${dev.owner.login}</a></td>
              <td>${dev.name}</td>
              <td>${dev.full_name}</td>
              <td>${dev.stargazers_count}</td></tr>
            `;
         });

         // Datatable render
         $(() => {  
          var customDataTable = $('#devtable').DataTable();

          customDataTable.destroy()

         var table = $(".display tbody")
         table.empty()
         table.append(jsonData)


         $('#devtable').DataTable();

        });

        this.isLoading = false  
      }
        
      },

      devDetail: async function(event) {

        const { data } = await axios.get(`https://api.github.com/users/${event}`)

        this.devDetails = {
          username: data.login,
          name: data.name,
          avatar: data.avatar_url,
          type: data.type,
          company: data.company,
          location: data.location,
          email: data.email,
          bio: data.bio,
          public_repo: data.public_repos,
          followers: data.followers,
          blog: data.blog,
          since: data.created_at
        }

        this.showDetail = true

      },

      addToFavorites: async function(event) {

        const { data } = await axios.post(`${this.baseUrl}/api/favorites`, {
          token: `cHJlcGFyYXRvZG9z`,
          username: this.devDetails.username,
          name: this.devDetails.name,
          avatar: this.devDetails.avatar,
          type: this.devDetails.type,
          company: this.devDetails.company,
          location: this.devDetails.location,
          email: this.devDetails.email,
          bio: this.devDetails.bio,
          public_repo: this.devDetails.public_repo,
          followers: this.devDetails.followers,
          blog: this.devDetails.blog,
          since: this.devDetails.since,
        })

        if(data.info === 'success') {
          swal("Yeah!", data.message, "success");
        } else {
          swal("Opppsss!", data.message, "error");
        }
      },
      
    },
    async mounted(){
      
    }
  })