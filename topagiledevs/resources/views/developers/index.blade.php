<div class="row">
    <div class="col-md-12">
        <div v-if="showDetail" class="box box-info dev-detail">
            <div class="box-header with-border">
                <h3 class="box-title">Detalhes do Desenvolvedor</h3>
                <button @click="showDetail = false" type="button" class="btn btn-primary btn-sm pull-right">Voltar</button>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="card">
                        <img height="150" :src="devDetails.avatar" class="img-circle" :alt="devDetails.name">
                            <div class="card-body">
                              <h2 class="card-title">@{{devDetails.name}}</h2>
                              <p class="card-text">@{{devDetails.bio}}</p>
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item"><strong>Usuário: </strong> @{{devDetails.username}}</li>
                                <li class="list-group-item"><strong>Tipo: </strong> @{{devDetails.type}}</li>
                                <li class="list-group-item"><strong>Empresa: </strong> @{{devDetails.company}}</li>
                                <li class="list-group-item"><strong>País: </strong> @{{devDetails.location}}</li>
                                <li class="list-group-item"><strong>E-mail: </strong> @{{devDetails.email}}</li>
                                <li class="list-group-item"><strong>Repos Público: </strong> @{{devDetails.public_repo}}</li>
                                <li class="list-group-item"><strong>Seguidores: </strong> @{{devDetails.followers}}</li>
                                <li class="list-group-item"><strong>Usuário desde: </strong> @{{devDetails.since}}</li>
                                <li class="list-group-item"><strong>Site/Blog: </strong> <a :href="devDetails.blog" class="card-link" target="_blank">@{{devDetails.blog}}</a></li>
                            </ul>

                            <div class="card-body">
                                <a @click="addToFavorites" class="cursor-dev">
                                    <i class="fa fa-star-o"></i>
                                    <span>Adicionar aos Favoritos</span>
                                </a>
                              </div>
                          </div>
                    </div>
                </div>
            </div>
        </div>

        <div v-show="!showDetail" class="box box-info dev-list">
            <div class="box-header with-border">
                <h3 class="box-title">Encontre desenvolvedores open source</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {{-- <form id="search_form"> --}}
            <!-- 'admin/search/list' -->

                <div class="box-body">

                    <div class="fields-group">

                        <div class="col-md-12">
                            <div class="form-group  ">

                                <label for="project" class="col-sm-2  control-label">Tipo de Projeto</label>

                                <div class="col-sm-12">

                                <input id="base_url" type="hidden" name="base_url" value="{{url('')}}">

                                    <select required v-model="project" class="form-control" style="width: 100%;" name="project" data-value="" tabindex="-1" aria-hidden="true">
                                        <option value="">Tipo de Projeto</option>
                                        <option value="website">website</option>
                                        <option value="mobile">mobile</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group  ">

                                <label for="language" class="col-sm-2  control-label">Linguagem</label>

                                <div class="col-sm-12">
                                    <select required v-model="language" class="form-control" style="width: 100%;" name="language" data-value="" tabindex="-1" aria-hidden="true">
                                        <option value="">Linguagem</option>
                                        <option value="php">php</option>
                                        <option value="python">python</option>
                                        <option value="swift">swift</option>
                                        <option value="dart">dart</option>
                                        <option value="java">java</option>
                                        <option value="kotlin">kotlin</option>
                                        <option value="javascript">javascript</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">

                                <label for="order" class="col-sm-2  control-label">Ordenação</label>

                                <div class="col-sm-12">
                                    <select required v-model="order" class="form-control" style="width: 100%;" name="order" data-value="" tabindex="-1" aria-hidden="true">
                                        <option value="">Ordenação</option>
                                        <option value="desc">Mais Popular</option>
                                        <option value="asc">Menos Popular</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="divider" class="col-sm-2  control-label">&nbsp;</label>
                                <div class="col-sm-12">
                                    <button 
                                        @click="getDevelopers" 
                                        v-bind:disabled="project === '' || language === '' || order === ''" 
                                        class="btn btn-primary">
                                            <img v-show="isLoading" height="16" src="{{ url('img/loading.gif') }}" alt="">
                                            Encontrar desenvolvedor
                                    </button>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                    
                    <br>
                    <div v-show="hasResult" class="col-md-12">
                        <h4>Veja o que encontramos:</h4>
                        <hr>
                        <table id="devtable" class="display" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Usuário</th>
                                    <th>Nome</th>
                                    <th>Projeto</th>
                                    <th>Estrelas</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Usuário</th>
                                    <th>Nome</th>
                                    <th>Projeto</th>
                                    <th>Estrelas</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>


                <!-- /.box-footer -->
            {{-- </form> --}}
        </div>

    </div>
</div>

