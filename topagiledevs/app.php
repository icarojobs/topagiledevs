<?php

namespace ;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class App/Models/Developer extends Model
{
    use SoftDeletes;

    protected $table = 'developers';
}
