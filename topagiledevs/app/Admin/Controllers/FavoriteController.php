<?php

namespace App\Admin\Controllers;

use App\Models\Favorite;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class FavoriteController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Favoritos')
            ->description('Aqui está os seus desenvolvedores favoritos')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detalhes')
            ->description('Detalhes do Desenvolvedor')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Atualização')
            ->description('Atualização do Desenvolvedor')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Novo Desenvolvedor')
            ->description('Crie um novo desenvolvedor manualmente')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new Favorite);

        //$grid->id('ID');
        $grid->username('username');
       // $grid->username('avatar_url');
        //$grid->bio('bio');
        $grid->type('type');
        //$grid->company('company');
        $grid->location('location');
        $grid->email('email');
        //$grid->public_repos('public_repos');
        //$grid->followers('followers');
        //$grid->since('since');
        //$grid->blog('blog');
        //$grid->created_at(trans('admin.created_at'));
        //$grid->updated_at(trans('admin.updated_at'));

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Favorite::findOrFail($id));

        //$show->id('ID');
        $show->username('username');
        $show->name('name');
        $show->avatar_url('avatar_url');
        $show->bio('bio');
        $show->type('type');
        $show->company('company');
        $show->location('location');
        $show->email('email');
        $show->public_repos('public_repos');
        $show->followers('followers');
        $show->since('since');
        $show->blog('blog');
        //$show->created_at(trans('admin.created_at'));
        //$show->updated_at(trans('admin.updated_at'));

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Favorite);

        //$form->display('ID');
        $form->text('username', 'username');
        $form->text('name', 'name');
        $form->text('avatar_url', 'avatar_url');
        $form->text('bio', 'bio');
        $form->text('type', 'type');
        $form->text('company', 'company');
        $form->text('location', 'location');
        $form->text('email', 'email');
        $form->text('public_repos', 'public_repos');
        $form->text('followers', 'followers');
        $form->text('since', 'since');
        $form->text('blog', 'blog');
        //$form->display(trans('admin.created_at'));
        //$form->display(trans('admin.updated_at'));

        return $form;
    }
}
