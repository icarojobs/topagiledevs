<?php

use Illuminate\Http\Request;
use App\Models\Favorite;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/favorites', function(Request $request) {
    
    $token = $request->token;

    if($token === base64_encode('preparatodos')) {
        $developer = Favorite::updateOrCreate(
            [
                'username' => $request->username
            ],
            [
                'name' => $request->name  ?? '',     
                'avatar_url' => $request->avatar  ?? '',
                'bio' => $request->bio  ?? '',
                'type' => $request->type  ?? '',
                'company' => $request->company  ?? '',
                'location' => $request->location  ?? '',
                'email' => $request->email  ?? '',
                'public_repos' => $request->public_repo  ?? '',
                'followers' => $request->followers  ?? '',
                'since' => $request->since  ?? '',
                'blog' => $request->blog  ?? '',
            ]
        );

        if($developer) {
            return [
                'username' => $request->username,
                'info' => 'success',
                'message' => "Desenvolvedor {$request->username} adicionado com sucesso!"
            ];
        }
        
        return [
            'info' => 'error',
            'message' => 'Erro ao inserir desenvolvedor. Tente novamente.'
        ];
    }

    return [
        'info' => 'error',
        'message' => 'Requisição não autorizada.'
    ];

});