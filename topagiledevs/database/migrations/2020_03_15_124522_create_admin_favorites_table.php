<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminFavoritesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_favorites', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username')->unique();
            $table->longText('bio')->nullable();
            $table->string('type')->nullable();
            $table->string('company')->nullable();
            $table->string('location')->nullable();
            $table->string('email')->nullable();
            $table->string('public_repos')->nullable();
            $table->string('followers')->nullable();
            $table->string('since')->nullable();
            $table->string('blog')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_favorites');
    }
}
