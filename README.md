# TOP AGILE DEVS
> The top 500 developers from GitHub!

This is an open source project developed with Laravel technology.

DEMO:
http://www.mob2you.com.br/topagiledevs/



How to run the project:
1. Download open source code at Gitlab:
    => $ git clone https://gitlab.com/icarojobs/topagiledevs.git

OBS.: The nexts steps must be executed into /topagiledevs directory:
 => $ cd topagiledevs

2. Run the composer install to install all vendors dependencies:
    => $ composer install

3. Run de migrations:
    => $ php artisan migrate 

3. For php/laravel dev, just run artisan serve:
    => $ php artisan serve

4. Open in your browser http://127.0.0.1:8000 and Enjoy!    

ADMIN AREA:
http://127.0.0.1:8080/admin
default user: preparatodos
default pass: preparatodos
